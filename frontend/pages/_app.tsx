import App from 'next/app'
import Layout from '../components/layout'

export default class MyApp extends App {
    render(): JSX.Element {
        return <Layout>{super.render()}</Layout>
    }
}
