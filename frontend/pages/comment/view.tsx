import React, { useEffect, useState } from 'react'
import { CircularProgress, Divider, Grid, List, Typography } from '@material-ui/core'
import axios from 'axios'
import CommentForm from './new'
import Comment from '../../components/comment'
import { buildTree, mapTree, TreeNode } from '../../util/tree'

interface Props {
    topic: string
    trigger?: number
}

enum TransitionState {
    Loading,
    Success,
    Error,
}

export default function View(props: Props) {
    const [state, setState] = useState(TransitionState.Loading)
    const [comment, setComment] = useState<TreeNode<{ content: { id: string; content: string } }>[]>([])
    const [replyTarget, setReplyTarget] = useState<{ id: string; content: string } | undefined>(undefined)
    const [errorMessage, setErrorMessage] = useState('')

    const getView = () => {
        if (props.topic === undefined) {
            return
        }
        setState(TransitionState.Loading)
        axios
            .get(`/api/topics/${props.topic}/comments`)
            .then((result) => {
                if (200 <= result.status && result.status < 300) {
                    setState(TransitionState.Success)
                    setComment(buildTree(result.data))
                } else {
                    setState(TransitionState.Error)
                    setErrorMessage(result.statusText)
                }
            })
            .catch((error) => {
                setState(TransitionState.Error)
                setErrorMessage(error.toString())
            })
    }
    useEffect(getView, [props.topic, props.trigger])

    const component = (treeNode: TreeNode<{ content: { id: string; content: string } }>) =>
        treeNode.data !== undefined ? (
            treeNode.children.length > 0 ? (
                <Comment
                    key={treeNode?.data?.content?.id}
                    id={treeNode?.data?.content?.id}
                    content={treeNode?.data?.content?.content}
                    onClick={(target) => setReplyTarget(target)}
                >
                    {mapTree(treeNode.children, component)}
                </Comment>
            ) : (
                <Comment
                    key={treeNode?.data?.content?.id}
                    id={treeNode?.data?.content?.id}
                    content={treeNode?.data?.content?.content}
                    onClick={(target) => setReplyTarget(target)}
                />
            )
        ) : (
            <></>
        )

    return (
        <>
            {state === TransitionState.Success ? (
                comment.length > 0 ? (
                    <List>{mapTree(comment, component)}</List>
                ) : (
                    <Typography variant='inherit' color='textSecondary'>
                        まだコメントはついていません。
                    </Typography>
                )
            ) : state === TransitionState.Loading ? (
                <Grid container justify='center'>
                    <Grid item>
                        <CircularProgress />
                    </Grid>
                </Grid>
            ) : (
                <Typography variant='inherit' color='error'>
                    {errorMessage}
                </Typography>
            )}
            <Divider />
            <CommentForm topic={props.topic} comment={replyTarget} onSubmit={getView} onCommentReset={() => setReplyTarget(undefined)} />
        </>
    )
}
