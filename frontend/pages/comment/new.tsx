import React, { FormEvent, useState } from 'react'
import { Backdrop, Button, CircularProgress, createStyles, Grid, IconButton, makeStyles, Snackbar, TextField, Theme, Typography } from '@material-ui/core'
import { Clear as ClearIcon, Close as CloseIcon } from '@material-ui/icons'
import axios from 'axios'

const styles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.appBar - 1,
        },
        p: {
            margin: '0.5rem auto',
        },
    })
)

enum TransitionState {
    Input,
    Sending,
}

interface Props {
    /** コメントに紐づけるトピックID */
    topic: string
    /** 新しいコメントの親のIDと内容 */
    comment?: { id: string; content: string }
    /** 新しいコメントの投稿に成功した後に呼び出されるイベントハンドラ */
    onSubmit?: () => void
    /** 新しいコメントの親の設定を解除する時に呼び出されるイベントハンドラ */
    onCommentReset?: () => void
}

export default function New(props: Props) {
    const classes = styles()

    const [state, setState] = useState(TransitionState.Input)
    const [comment, setComment] = useState('')
    const [errorOpen, setErrorOpen] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        setState(TransitionState.Sending)
        const data: {
            comment: string
            parent?: string
        } = {
            comment: comment,
        }
        if (props.comment !== undefined) {
            data.parent = props.comment.id
        }
        axios
            .post(`/api/topics/${props.topic}/comments`, data)
            .then((result) => {
                if (200 <= result.status && result.status < 300) {
                    setState(TransitionState.Input)
                    setComment('')
                    if (props.onSubmit !== undefined) {
                        props.onSubmit()
                    }
                    if (props.onCommentReset !== undefined) {
                        props.onCommentReset()
                    }
                } else {
                    setState(TransitionState.Input)
                    setErrorMessage(result.statusText)
                    setErrorOpen(true)
                }
            })
            .catch((error) => {
                setState(TransitionState.Input)
                setErrorMessage(error.toString())
                setErrorOpen(true)
            })
    }

    return (
        <>
            <Typography variant='h3'>新規コメント</Typography>
            <form onSubmit={handleSubmit}>
                {props.comment !== undefined ? (
                    <Grid container spacing={2}>
                        <Grid item style={{ flexGrow: 0 }}>
                            <IconButton
                                onClick={
                                    props.onCommentReset !== undefined
                                        ? props.onCommentReset
                                        : () => {
                                              return
                                          }
                                }
                            >
                                <ClearIcon />
                            </IconButton>
                        </Grid>
                        <Grid item style={{ flexGrow: 1 }}>
                            <TextField label='返信先' multiline fullWidth inputProps={{ readOnly: true }} value={props.comment.content} className={classes.p} />
                        </Grid>
                    </Grid>
                ) : (
                    <></>
                )}
                <TextField
                    label='コメント内容'
                    multiline
                    fullWidth
                    disabled={state !== TransitionState.Input}
                    value={comment}
                    onChange={(event) => setComment(event.target.value)}
                    className={classes.p}
                />
                <Button variant='contained' color='primary' type='submit' disabled={state !== TransitionState.Input} className={classes.p}>
                    送信
                </Button>
            </form>
            <Backdrop open={state === TransitionState.Sending} className={classes.backdrop}>
                <CircularProgress color='inherit' />
            </Backdrop>
            <Snackbar
                open={errorOpen}
                message={errorMessage}
                action={
                    <IconButton onClick={() => setErrorOpen(false)}>
                        <CloseIcon color='error' />
                    </IconButton>
                }
            />
        </>
    )
}
