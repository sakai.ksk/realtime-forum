import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { Card, CardContent, CircularProgress, createStyles, Grid, makeStyles, Theme, Typography } from '@material-ui/core'
import axios from 'axios'
import CommentView from '../comment/view'
import Subscriber from '../../components/subscriber'
import nl2br from '../../util/nl2br'

const styles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.appBar - 1,
        },
        adminOnly: {
            backgroundColor: theme.palette.primary.light,
        },
        root: {
            display: 'flex',
            justifyContent: 'flex-start',
            flexWrap: 'wrap',
        },
        chip: {
            margin: theme.spacing(1),
        },
        largeFont: {
            fontSize: '2em',
        },
    })
)

enum TransitionState {
    Loading,
    Success,
    Error,
}

export default function View() {
    const router = useRouter()
    const id = router.query?.id?.toString()

    const classes = styles()

    const [state, setState] = useState(TransitionState.Loading)
    const [title, setTitle] = useState<string | undefined>(undefined)
    const [question, setQuestion] = useState<string | undefined>(undefined)
    const [answer, setAnswer] = useState<string | undefined>(undefined)
    const [errorMessage, setErrorMessage] = useState('')
    const [commentViewRefreshTrigger, setCommentViewRefreshTrigger] = useState(0) // 邪道な方法だ……。

    useEffect(() => {
        if (id === undefined) {
            return
        }
        axios
            .get(`/api/topics/${id}`)
            .then((result) => {
                if (200 <= result.status && result.status < 300) {
                    setState(TransitionState.Success)
                    setTitle(result.data.title?.content)
                    setQuestion(result.data.question?.content)
                    setAnswer(result.data.answer?.content)
                } else {
                    setState(TransitionState.Error)
                    setErrorMessage(result.statusText)
                }
            })
            .catch((error) => {
                setState(TransitionState.Error)
                setErrorMessage(error.toString())
            })
    }, [id])

    return (
        <>
            <Head>
                <title>トピック表示(id={id}) - Realtime Knowledge Database</title>
            </Head>
            {state === TransitionState.Success ? (
                <>
                    <Typography variant='h1'>
                        {title ?? (
                            <Typography variant='inherit' color='textSecondary'>
                                表題未設定
                            </Typography>
                        )}
                    </Typography>
                    <Grid container spacing={2}>
                        <Grid item xs={12} lg container spacing={2}>
                            <Grid item xs={12} container>
                                <Grid item xs={1}>
                                    <Typography variant='h2'>Q</Typography>
                                </Grid>
                                <Grid item xs>
                                    <Card>
                                        <CardContent>
                                            <Typography variant='body1'>
                                                {question !== undefined ? (
                                                    nl2br(question)
                                                ) : (
                                                    <Typography variant='inherit' color='error'>
                                                        エラー：質問内容がありません。
                                                    </Typography>
                                                )}
                                            </Typography>
                                        </CardContent>
                                    </Card>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container>
                                <Grid item xs={1}>
                                    <Typography variant='h2'>A</Typography>
                                </Grid>
                                <Grid item xs>
                                    <Card>
                                        <CardContent>
                                            <Typography variant='body1'>
                                                {answer !== undefined ? (
                                                    nl2br(answer)
                                                ) : (
                                                    <Typography variant='inherit' color='textSecondary'>
                                                        まだ回答はついていません。
                                                    </Typography>
                                                )}
                                            </Typography>
                                        </CardContent>
                                    </Card>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container>
                                <Grid item xs={1}>
                                    <Typography variant='h2'>C</Typography>
                                </Grid>
                                <Grid item xs>
                                    <Card>
                                        <CardContent>
                                            <CommentView topic={id} trigger={commentViewRefreshTrigger} />
                                        </CardContent>
                                    </Card>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} lg={4}>
                            <Card className={classes.adminOnly}>
                                <CardContent>
                                    <Typography variant='h5'>管理用</Typography>
                                    <Grid container>
                                        <Grid item xs={1}>
                                            <Typography variant='h2'>D</Typography>
                                        </Grid>
                                        <Grid item xs>
                                            <Subscriber
                                                topic={id}
                                                onReceive={() => {
                                                    setCommentViewRefreshTrigger(commentViewRefreshTrigger + 1)
                                                }}
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </>
            ) : state === TransitionState.Loading ? (
                <Grid container justify='center'>
                    <Grid item>
                        <CircularProgress />
                    </Grid>
                </Grid>
            ) : (
                <Typography variant='inherit' color='error'>
                    {errorMessage}
                </Typography>
            )}
        </>
    )
}
