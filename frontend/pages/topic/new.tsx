import React, { FormEvent, useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { Backdrop, Button, CircularProgress, createStyles, IconButton, makeStyles, Snackbar, TextField, Theme, Typography } from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'
import axios from 'axios'
import { UAParser } from 'ua-parser-js'

const styles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.appBar - 1,
        },
        headingInCard: {
            marginTop: 0,
        },
        root: {
            display: 'flex',
            justifyContent: 'flex-start',
            flexWrap: 'wrap',
        },
        chip: {
            margin: theme.spacing(1),
        },
        largeFont: {
            fontSize: '2em',
        },
    })
)

enum TransitionState {
    Input,
    Sending,
}

export default function New() {
    const ua = new UAParser(typeof navigator !== 'undefined' ? navigator.userAgent : '')

    const classes = styles()

    const router = useRouter()

    const [state, setState] = useState(TransitionState.Input)
    const [question, setQuestion] = useState('')
    const [device, setDevice] = useState(((d) => d.type)(ua.getDevice()) ?? '')
    const [os, setOs] = useState(((o, c) => [c.architecture, o.name, o.version].join(' ').trim())(ua.getOS(), ua.getCPU()) ?? '')
    const [browser, setBrowser] = useState(((b, e) => [e.name, b.name, b.version].join(' ').trim())(ua.getBrowser(), ua.getEngine()) ?? '')
    const [errorOpen, setErrorOpen] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        setState(TransitionState.Sending)
        axios
            .post('/api/topics', {
                question: question,
            })
            .then((result) => {
                if (200 <= result.status && result.status < 300) {
                    router.push(`/topic?id=${result.data.id}`)
                } else {
                    setState(TransitionState.Input)
                    setErrorMessage(result.statusText)
                    setErrorOpen(true)
                }
            })
            .catch((error) => {
                setState(TransitionState.Input)
                setErrorMessage(error.toString())
                setErrorOpen(true)
            })
    }

    return (
        <>
            <Head>
                <title>質問投稿フォーム - Realtime Knowledge Database</title>
            </Head>
            <Typography variant='h1'>質問投稿フォーム</Typography>
            <form onSubmit={handleSubmit}>
                <p>ご質問内容をご記入ください。</p>
                <TextField
                    variant='outlined'
                    label='ご質問'
                    multiline
                    rows={5}
                    fullWidth
                    disabled={state !== TransitionState.Input}
                    value={question}
                    onChange={(event) => setQuestion(event.target.value)}
                />
                <p>
                    以下はご使用中の環境についての情報の入力欄です。
                    <s>原因調査に役立てるため、できるだけ自動入力のままご送信していただくようお願いします。望まない場合は内容を消してください。</s>
                    （注: 現時点では`ua-parser-js`ライブラリの検証のために表示しているだけで、サーバへ送信されません。）
                </p>
                <TextField
                    label='種類'
                    fullWidth
                    disabled={state !== TransitionState.Input}
                    value={device}
                    onChange={(event) => setDevice(event.target.value)}
                />
                <TextField label='OS' fullWidth disabled={state !== TransitionState.Input} value={os} onChange={(event) => setOs(event.target.value)} />
                <TextField
                    label='ブラウザ'
                    fullWidth
                    disabled={state !== TransitionState.Input}
                    value={browser}
                    onChange={(event) => setBrowser(event.target.value)}
                />
                <Button variant='contained' color='primary' type='submit' disabled={state !== TransitionState.Input}>
                    送信
                </Button>
            </form>
            <Backdrop open={state === TransitionState.Sending} className={classes.backdrop}>
                <CircularProgress color='inherit' />
            </Backdrop>
            <Snackbar
                open={errorOpen}
                message={errorMessage}
                action={
                    <IconButton onClick={() => setErrorOpen(false)}>
                        <CloseIcon />
                    </IconButton>
                }
            />
        </>
    )
}
