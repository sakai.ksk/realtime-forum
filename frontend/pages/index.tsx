import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { Button, Card, CardContent, Chip, createStyles, Grid, List, ListItem, ListItemText, makeStyles, Theme, Typography } from '@material-ui/core'
import ButtonLink from '../components/link'

const styles = makeStyles((theme: Theme) =>
    createStyles({
        headingInCard: {
            marginTop: 0,
        },
        root: {
            display: 'flex',
            justifyContent: 'flex-start',
            flexWrap: 'wrap',
        },
        chip: {
            margin: theme.spacing(1),
        },
        largeFont: {
            fontSize: '2em',
        },
    })
)

export default function Index() {
    const classes = styles()

    return (
        <>
            <Head>
                <title>Realtime Knowledge Database</title>
            </Head>
            <Typography variant='h1'>Realtime Knowledge Database</Typography>
            <p>リアルタイムに更新されるQ&Aフォーラム（将来の機能拡張を見越して画面を作っているため、クリックしても何も起こらないところが多々あります。）</p>
            <Typography variant='h2'>回答を検索</Typography>
            <Grid container spacing={2}>
                <Grid item xs={12} md={6} lg={3}>
                    <Card>
                        <CardContent>
                            <Typography variant='h6' className={classes.headingInCard}>
                                閲覧の多い順(工事中)
                            </Typography>
                            <List>
                                <ListItem button component={ButtonLink} href='/topic/view?id=1'>
                                    <ListItemText primary='トピック1' />
                                </ListItem>
                                <ListItem button component={ButtonLink} href='/topic/view?id=2'>
                                    <ListItemText primary='トピック2' />
                                </ListItem>
                                <ListItem button component={ButtonLink} href='/topic/view?id=3'>
                                    <ListItemText primary='トピック3' />
                                </ListItem>
                                <ListItem button component={ButtonLink} href='/topic/view?id=4'>
                                    <ListItemText primary='トピック4' />
                                </ListItem>
                            </List>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} md={6} lg={3}>
                    <Card>
                        <CardContent>
                            <Typography variant='h6' className={classes.headingInCard}>
                                更新の新しい順(工事中)
                            </Typography>
                            <List>
                                <ListItem button component={ButtonLink} href='/topic/view?id=5'>
                                    <ListItemText primary='トピック5' />
                                </ListItem>
                                <ListItem button component={ButtonLink} href='/topic/view?id=6'>
                                    <ListItemText primary='トピック6' />
                                </ListItem>
                                <ListItem button component={ButtonLink} href='/topic/view?id=7'>
                                    <ListItemText primary='トピック7' />
                                </ListItem>
                                <ListItem button component={ButtonLink} href='/topic/view?id=8'>
                                    <ListItemText primary='トピック8' />
                                </ListItem>
                            </List>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} md={6} lg={3}>
                    <Card>
                        <CardContent>
                            <Typography variant='h6' className={classes.headingInCard}>
                                カテゴリで絞り込み(まだ使えません)
                            </Typography>
                            <div className={classes.root}>
                                <Chip label='カテゴリ1' className={classes.chip} />
                                <Chip label='カテゴリ2' className={classes.chip} />
                                <Chip label='カテゴリ3' className={classes.chip} />
                                <Chip label='カテゴリ4' className={classes.chip} />
                            </div>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} md={6} lg={3}>
                    <Card>
                        <CardContent>
                            <Typography variant='h6' className={classes.headingInCard}>
                                タグで絞り込み(まだ使えません)
                            </Typography>
                            <div className={classes.root}>
                                <Chip label='タグ1' className={classes.chip} />
                                <Chip label='タグ2' className={classes.chip} />
                                <Chip label='タグ3' className={classes.chip} />
                                <Chip label='タグ4' className={classes.chip} />
                            </div>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <Typography variant='h2'>質問を投稿</Typography>
            <Grid container spacing={2}>
                <Grid item xs={12} md={12}>
                    <Link href='/topic/new'>
                        <Button variant='contained' color='primary' size='large' fullWidth={true}>
                            <span className={classes.largeFont}>【質問投稿フォーム】</span>
                        </Button>
                    </Link>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Link href='/topic/new'>
                        <Button variant='outlined' color='primary' size='large' fullWidth={true}>
                            回答が不要な場合(まだ使えません)
                        </Button>
                    </Link>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Link href='/topic/new'>
                        <Button variant='outlined' color='primary' size='large' fullWidth={true}>
                            知見を共有する場合(まだ使えません)
                        </Button>
                    </Link>
                </Grid>
            </Grid>
        </>
    )
}
