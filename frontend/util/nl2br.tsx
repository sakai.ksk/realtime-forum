import React from 'react'

export default function nl2br(str: string) {
    return str.split('\n').map((val, ind) => (
        <React.Fragment key={ind}>
            {val}
            <br />
        </React.Fragment>
    ))
}
