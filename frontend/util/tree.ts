export type AdjacencyListNode<T> = {
    id: number
    parent_id: number | null
} & T

export type TreeNode<T> = {
    id: number
    children: TreeNode<T>[]
    data?: T
}

export function buildTree<T>(array: AdjacencyListNode<T>[]): TreeNode<T>[] {
    const initialStore: {
        catalog: TreeNode<T>[]
        tree: TreeNode<T>[]
    } = {
        catalog: [],
        tree: [],
    }
    return array.reduce((store, value) => {
        const existingCurrentNode = store.catalog[value.id]
        if (existingCurrentNode === undefined) {
            store.catalog[value.id] = {
                id: value.id,
                children: [],
                data: value,
            }
        } else {
            existingCurrentNode.data = value
        }
        const storedCurrentNode = store.catalog[value.id]
        if (value.parent_id !== null) {
            const existingParentNode = store.catalog[value.parent_id]
            if (existingParentNode === undefined) {
                store.catalog[value.parent_id] = {
                    id: value.parent_id,
                    children: [storedCurrentNode],
                }
            } else {
                existingParentNode.children.push(storedCurrentNode)
            }
        } else {
            store.tree.push(storedCurrentNode)
        }
        return store
    }, initialStore).tree
}

export function mapTree<T, U>(tree: TreeNode<T>[], mapFunction: (treeNode: TreeNode<T>) => U) {
    return tree.map(mapFunction)
}
