import React, { PropsWithChildren } from 'react'
import { createStyles, IconButton, List, ListItem, ListItemText, makeStyles, Theme } from '@material-ui/core'
import { Reply as ReplyIcon } from '@material-ui/icons'
import nl2br from '../util/nl2br'

const styles = makeStyles((theme: Theme) =>
    createStyles({
        commentBox: {
            margin: '1rem auto',
            borderLeftColor: theme.palette.grey.A700,
            borderLeftStyle: 'solid',
            borderLeftWidth: '0.25rem',
            '&:first-child': {
                marginTop: 0,
            },
            '&:last-child': {
                marginBottom: 0,
            },
        },
        commentText: {},
    })
)

interface Props {
    id: string
    content: string
    onClick?: (obj: { id: string; content: string }) => void
}

export default function Comment(props: PropsWithChildren<Props>) {
    const classes = styles()

    return (
        <>
            <ListItem className={classes.commentBox}>
                <div>
                    <ListItemText className={classes.commentText}>{nl2br(props.content)}</ListItemText>
                    <IconButton
                        size='small'
                        onClick={() => {
                            if (props.onClick !== undefined) {
                                props.onClick({ id: props.id, content: props.content })
                            }
                        }}
                    >
                        <ReplyIcon />
                    </IconButton>
                    {props.children !== undefined ? <List>{props.children}</List> : <></>}
                </div>
            </ListItem>
        </>
    )
}
