import React, { PropsWithChildren } from 'react'
import Head from 'next/head'
import {
    AppBar,
    Button,
    Container,
    createMuiTheme,
    createStyles,
    CssBaseline,
    Divider,
    Grid,
    Hidden,
    IconButton,
    makeStyles,
    Menu,
    MenuItem,
    Theme,
    ThemeProvider,
    Toolbar,
    Typography,
} from '@material-ui/core'
import { Menu as MenuIcon, MoreVert as MoreVertIcon } from '@material-ui/icons'
import Sidebar from './sidebar'

const theme = createMuiTheme({
    typography: {
        h1: {
            margin: '1rem auto',
            fontSize: '300%',
            fontWeight: 700,
        },
        h2: {
            margin: '1rem auto',
            fontSize: '200%',
            fontWeight: 700,
        },
        h3: {
            margin: '1rem auto',
            fontSize: '150%',
            fontWeight: 600,
        },
        h4: {
            margin: '1rem auto',
            fontSize: '125%',
            fontWeight: 600,
        },
        h5: {
            margin: '1rem auto',
            fontSize: '112.5%',
            fontWeight: 500,
        },
        h6: {
            margin: '1rem auto',
            fontSize: '106.25%',
            fontWeight: 500,
        },
    },
})

const styles = makeStyles((theme: Theme) =>
    createStyles({
        grow: {
            flexGrow: 1,
        },
    })
)

export default function Layout({ children }: PropsWithChildren<unknown>): JSX.Element {
    const classes = styles()

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget)
    }
    const handleClose = () => {
        setAnchorEl(null)
    }

    return (
        <React.Fragment>
            <Head>
                <title>Realtime Knowledge Database</title>
                <meta name='viewport' content='initial-scale=1, width=device-width' />
            </Head>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <AppBar position='static'>
                    <Toolbar>
                        <IconButton edge='start' color='inherit'>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant='h4'>Realtime Knowledge Database</Typography>
                        <div className={classes.grow} />
                        <Button color='inherit'>検索</Button>
                        <Button color='inherit'>投稿</Button>
                        <Button color='inherit'>ログイン</Button>
                        <Divider orientation='vertical' flexItem light />
                        <Button color='inherit'>設定</Button>
                        <Button color='inherit'>ヘルプ</Button>
                        <IconButton edge='start' color='inherit' onClick={handleClick}>
                            <MoreVertIcon />
                        </IconButton>
                        <Menu id='menu' anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
                            <MenuItem onClick={handleClose}>検索</MenuItem>
                            <MenuItem onClick={handleClose}>投稿</MenuItem>
                            <MenuItem onClick={handleClose}>ログイン</MenuItem>
                            <Divider />
                            <MenuItem onClick={handleClose}>設定</MenuItem>
                            <MenuItem onClick={handleClose}>ヘルプ</MenuItem>
                        </Menu>
                    </Toolbar>
                </AppBar>
                <Container>
                    <Grid container spacing={2}>
                        <Grid item xs={12} md={2}>
                            <Hidden smDown implementation='css'>
                                <Sidebar />
                            </Hidden>
                        </Grid>
                        <Grid item xs={12} sm>
                            <main>{children}</main>
                        </Grid>
                    </Grid>
                </Container>
            </ThemeProvider>
        </React.Fragment>
    )
}
