import React from 'react'
import { Collapse, List, ListItem, ListItemText, ListSubheader } from '@material-ui/core'
import { ExpandLess, ExpandMore } from '@material-ui/icons'
import ButtonLink from './link'

export default function Sidebar() {
    const [open, setOpen] = React.useState(false)

    return (
        <>
            <List>
                <ListItem button component={ButtonLink} href='/'>
                    <ListItemText primary='ホーム' />
                </ListItem>
                <ListItem button component={ButtonLink} href='/topic/new'>
                    <ListItemText primary='質問投稿フォーム' />
                </ListItem>
                <ListItem
                    button
                    onClick={() => {
                        setOpen(!open)
                    }}
                >
                    <ListItemText primary='その他機能' />
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={open} timeout='auto' unmountOnExit>
                    <List disablePadding></List>
                </Collapse>
            </List>
            <List subheader={<ListSubheader>管理者専用</ListSubheader>}></List>
        </>
    )
}
