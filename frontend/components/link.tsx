import React, { PropsWithChildren } from 'react'
import Link, { LinkProps } from 'next/link'

interface ButtonLinkProps extends LinkProps {
    className?: string | undefined
}

export default function ButtonLink({ children, className, ...otherProps }: PropsWithChildren<ButtonLinkProps>) {
    return (
        <Link {...otherProps}>
            <a className={className}>{children}</a>
        </Link>
    )
}
