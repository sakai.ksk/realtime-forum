import React, { useEffect, useState } from 'react'
import { IconButton, Snackbar, Typography } from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'
import Echo from 'laravel-echo'
import io from 'socket.io-client'

interface Props {
    topic: string
    onReceive?: () => void
}

enum SubscriptionState {
    None,
    Connected,
    Error,
}

export default function View(props: Props) {
    const [state, setState] = useState(SubscriptionState.None)
    const [notificationOpen, setNotificationOpen] = useState(false)
    const [notificationMessage, setNotificationMessage] = useState('')
    const [subscription, setSubscription] = useState<Echo | null>(null)

    const subscribe = (): (() => void) => {
        if (subscription === null) {
            // Laravel Echo クライアントを作成
            const echo = new Echo({
                broadcaster: 'socket.io',
                host: typeof window !== 'undefined' ? window.location.host : 'localhost',
                client: io,
            })
            // channel を購読
            echo.channel('new-comment').listen('.App\\Events\\NewCommentEvent', (e: { topic_id: string }) => {
                if (props.topic === e.topic_id.toString() && props.onReceive !== undefined) {
                    props.onReceive()
                } else {
                    setNotificationMessage(`更新通知: id=${e.topic_id}`)
                    setNotificationOpen(true)
                }
            })
            // socket.io 例外処理 (Laravel Echo クライアントで出来ればいいのに……。)
            const socket: SocketIOClient.Socket = echo.connector.socket
            socket.on('connect_error', () => {
                echo.disconnect()
                setState(SubscriptionState.Error)
                setNotificationMessage('ERROR: 自動更新が切断されました。再接続するにはページを更新してください。')
                setNotificationOpen(true)
            })
            // React Component state に反映
            setState(SubscriptionState.Connected)
            setSubscription(echo)
        }
        return unsubscribe
    }
    const unsubscribe = () => {
        if (subscription !== null) {
            setState(SubscriptionState.Error)
            subscription.disconnect()
        }
    }
    useEffect(subscribe, [subscription])

    return (
        <>
            <Typography variant='body2'>
                {state === SubscriptionState.None
                    ? '未接続'
                    : state === SubscriptionState.Connected
                    ? '接続中'
                    : state === SubscriptionState.Error
                    ? 'エラー'
                    : '未定義'}
            </Typography>
            <Snackbar
                open={notificationOpen}
                message={notificationMessage}
                action={
                    <IconButton onClick={() => setNotificationOpen(false)}>
                        <CloseIcon color='error' />
                    </IconButton>
                }
                autoHideDuration={5000}
                onClose={() => setNotificationOpen(false)}
            />
        </>
    )
}
