<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->unsignedBigInteger('topic_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->integer('position');
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('topic_id')->references('id')->on('topics');

            $table->foreign('parent_id')
                ->references('id')
                ->on('comments')
                ->onDelete('set null');
        });

        Schema::create('comment_closure', function (Blueprint $table) {
            $table->bigIncrements('closure_id');

            $table->unsignedBigInteger('ancestor');
            $table->unsignedBigInteger('descendant');
            $table->unsignedBigInteger('depth');

            $table->foreign('ancestor')
                ->references('id')
                ->on('comments')
                ->onDelete('cascade');

            $table->foreign('descendant')
                ->references('id')
                ->on('comments')
                ->onDelete('cascade');
        });

        Schema::create('comment_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('content');
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->unsignedBigInteger('comment_id');
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('comment_id')->references('id')->on('comments');
        });
    }

    public function down()
    {
        Schema::dropIfExists('comment_contents');
        Schema::dropIfExists('comment_closure');
        Schema::dropIfExists('comments');
    }
}
