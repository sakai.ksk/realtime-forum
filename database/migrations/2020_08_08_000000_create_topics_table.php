<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
        });

        Schema::create('topic_titles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('content');
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->unsignedBigInteger('topic_id');
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('topic_id')->references('id')->on('topics');
        });

        Schema::create('topic_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('type');
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->unsignedBigInteger('topic_id');
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('topic_id')->references('id')->on('topics');
        });

        Schema::create('topic_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('status');
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->unsignedBigInteger('topic_id');
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('topic_id')->references('id')->on('topics');
        });

        Schema::create('topic_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('content');
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->unsignedBigInteger('topic_id');
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('topic_id')->references('id')->on('topics');
        });

        Schema::create('topic_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('content');
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->unsignedBigInteger('topic_id');
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('topic_id')->references('id')->on('topics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_answers', function (Blueprint $table) {
            $table->dropForeign(['creator_id']);
            $table->dropForeign(['topic_id']);

            $table->dropIfExists();
        });

        Schema::table('topic_questions', function (Blueprint $table) {
            $table->dropForeign(['creator_id']);
            $table->dropForeign(['topic_id']);

            $table->dropIfExists();
        });

        Schema::table('topic_statuses', function (Blueprint $table) {
            $table->dropForeign(['creator_id']);
            $table->dropForeign(['topic_id']);

            $table->dropIfExists();
        });

        Schema::table('topic_types', function (Blueprint $table) {
            $table->dropForeign(['creator_id']);
            $table->dropForeign(['topic_id']);

            $table->dropIfExists();
        });

        Schema::table('topic_titles', function (Blueprint $table) {
            $table->dropForeign(['creator_id']);
            $table->dropForeign(['topic_id']);

            $table->dropIfExists();
        });

        Schema::table('topics', function (Blueprint $table) {
            $table->dropForeign(['creator_id']);

            $table->dropIfExists();
        });
    }
}
