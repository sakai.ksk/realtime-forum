<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use App\Models\Topic\{Question, Status, Type};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Topic::query()->with(['creator', 'title', 'type', 'status', 'question', 'answer'])->get()->map([Topic::class, 'print']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        if (!$request->has('question')) {
            return abort(400, '`question` is empty');
        }

        DB::beginTransaction();
        try {
            $topic = new Topic();
            $topic->saveOrFail();

            $question = new Question([
                'content' => $request->get('question'),
            ]);
            $question->topic()->associate($topic);
            $question->saveOrFail();

            $type = new Type([
                'type' => 0,
            ]);
            $type->topic()->associate($topic);
            $type->saveOrFail();

            $status = new Status([
                'status' => 0,
            ]);
            $status->topic()->associate($topic);
            $status->saveOrFail();

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return $this->show($topic->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        return Topic::print(Topic::query()->with(['creator', 'title', 'type', 'status', 'question', 'answer'])->findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        if (!$request->has('question')) {
            return abort(400, '`question` is empty');
        }

        DB::beginTransaction();
        try {
            $topic = Topic::query()->findOrFail($id);

            $question = new Question([
                'content' => $request->get('question'),
            ]);
            $question->saveOrFail();
            $question->topic()->associate($topic);

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Topic::destroy($id);
    }
}
