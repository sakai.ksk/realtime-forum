<?php

namespace App\Http\Controllers;

use App\Events\NewCommentEvent;
use App\Models\Comment;
use App\Models\Comment\Content;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  int  $topic_id
     */
    public function index($topic_id)
    {
        return Comment::query()
            ->where('topic_id', '=', $topic_id)
            ->with(['creator', 'topic', 'content'])
            ->get()
            ->map([Comment::class, 'print']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $topic_id
     */
    public function store(Request $request, $topic_id)
    {
        if (!$request->has('comment')) {
            return abort(400, '`comment` is empty');
        }

        DB::beginTransaction();
        try {
            $topic = Topic::query()->findOrFail($topic_id);

            $comment = new Comment();
            $comment->topic()->associate($topic);
            if ($request->has('parent')) {
                $parent = Comment::query()->find($request->get('parent'));
                if ($parent !== null) {
                    $comment->parent_id = $parent->id;
                }
            }
            $comment->saveOrFail();

            $content = new Content([
                'content' => $request->get('comment'),
            ]);
            $content->comment()->associate($comment);
            $content->saveOrFail();

            broadcast(new NewCommentEvent($topic_id, $comment));

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return $this->show($topic_id, $comment->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $topic_id
     * @param  int  $id
     */
    public function show($topic_id, $id)
    {
        return Comment::print(
            Comment::query()
                ->where('topic_id', '=', $topic_id)
                ->with(['creator', 'topic', 'content'])
                ->findOrFail($id)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $topic_id
     * @param  int  $id
     */
    public function update(Request $request, $topic_id, $id)
    {
        return abort(405, 'コメントは変更できません。');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $topic_id
     * @param  int  $id
     */
    public function destroy($topic_id, $id)
    {
        return abort(405, 'コメントは削除できません。');
    }
}
