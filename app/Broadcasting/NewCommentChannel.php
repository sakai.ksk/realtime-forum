<?php

namespace App\Broadcasting;

use App\User;
use Illuminate\Broadcasting\Channel;

class NewCommentChannel extends Channel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct('new-comment');
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\User  $user
     * @return array|bool
     */
    public function join(User $user)
    {
        return true;
    }
}
