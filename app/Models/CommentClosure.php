<?php
namespace App\Models;

use Franzose\ClosureTable\Models\ClosureTable;

class CommentClosure extends ClosureTable
{
    protected $table = 'comment_closure';
}
