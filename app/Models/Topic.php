<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = 'topics';

    public function creator() {
        return $this->belongsTo('App\User');
    }

    public function title() {
        return $this->hasMany('App\Models\Topic\Title');
    }

    public function type() {
        return $this->hasMany('App\Models\Topic\Type');
    }

    public function status() {
        return $this->hasMany('App\Models\Topic\Status');
    }

    public function question() {
        return $this->hasMany('App\Models\Topic\Question');
    }

    public function answer() {
        return $this->hasMany('App\Models\Topic\Answer');
    }

    public function comments() {
        return $this->hasMany('App\Models\Comment');
    }

    public static function print(Topic $topic) {
        return [
            'id' => $topic->id,
            'creator' => $topic->creator,
            'title' => $topic->title->sortByDesc('updated_at')->first(),
            'type' => $topic->type->sortByDesc('updated_at')->first(),
            'status' => $topic->status->sortByDesc('updated_at')->first(),
            'question' => $topic->question->sortByDesc('updated_at')->first(),
            'answer' => $topic->answer->sortByDesc('updated_at')->first(),
            'created_at' => $topic->created_at,
            'updated_at' => $topic->updated_at,
        ];
    }
}
