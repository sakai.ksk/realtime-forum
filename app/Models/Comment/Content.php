<?php

namespace App\Models\Comment;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'comment_contents';

    protected $fillable = ['content'];

    public function comment() {
        return $this->belongsTo('App\Models\Topic');
    }
}
