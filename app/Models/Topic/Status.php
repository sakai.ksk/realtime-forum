<?php

namespace App\Models\Topic;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'topic_statuses';

    protected $fillable = ['status'];

    public function creator() {
        return $this->belongsTo('App\User');
    }

    public function topic() {
        return $this->belongsTo('App\Models\Topic');
    }
}
