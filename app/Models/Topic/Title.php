<?php

namespace App\Models\Topic;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    protected $table = 'topic_titles';

    protected $fillable = ['content'];

    public function creator() {
        return $this->belongsTo('App\User');
    }

    public function topic() {
        return $this->belongsTo('App\Models\Topic');
    }
}
