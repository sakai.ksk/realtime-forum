<?php

namespace App\Models\Topic;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'topic_types';

    protected $fillable = ['type'];

    public function creator() {
        return $this->belongsTo('App\User');
    }

    public function topic() {
        return $this->belongsTo('App\Models\Topic');
    }
}
