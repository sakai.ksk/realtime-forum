<?php

namespace App\Models;

use Franzose\ClosureTable\Models\Entity;

class Comment extends Entity
{
    protected $table = 'comments';

    protected $closure = 'App\Models\CommentClosure';

    public $timestamps = true;

    public function creator() {
        return $this->belongsTo('App\User');
    }

    public function topic() {
        return $this->belongsTo('App\Models\Topic');
    }

    public function content() {
        return $this->hasMany('App\Models\Comment\Content');
    }

    public static function print(Comment $comment) {
        return [
            'id' => $comment->id,
            'creator' => $comment->creator,
            'topic' => $comment->topic,
            'content' => $comment->content->sortByDesc('updated_at')->first(),
            'parent_id' => $comment->parent_id,
            'created_at' => $comment->created_at,
            'updated_at' => $comment->updated_at,
        ];
    }
}
